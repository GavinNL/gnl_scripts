# GNL Scripts


Just a handfull of useful scripts I have written to make my life easier. The following
dependencies are needed to run all these scripts:

* zenity
* eyed3
* ffmpeg
* cryfs
* docker
* python3

```
sudo apt install zenity eyed3 ffmpeg cryfs docker python3
```

Download a debian package for the scripts:

https://gitlab.com/GavinNL/gnl_scripts/-/jobs/artifacts/master/raw/artifacts/gnl-scripts.deb?job=build-gcc7


## gnl_record_audio

Record the audio being emitted from a audio device and save it to a wav file in your
$HOME/Music folder.

## gnl_set_bpm

Sets the BPM (beats per min) for an audio file. Nothing fancy here, I just have to
run this command quite often so I created a quick script for it.


## gnl_wav_to_mp3

Convert a wav file into an mp3  using ffmpeg.

```
gnl_wave_to_mp3  myfile.wav myfile.mp3
```

## gnl_www

Quickly host the contents of the current working directory as a web server using python3's http server module.  

## gnl_dropbox

Run a dropbox instance through a docker container. The container runs in `--restart always`
mode so that once you start it, you do not need to worry about it again.

```
gnl_dropbox $HOME/Dropbox
```

This will create a `Dropbox` in your home directory. This folder containers 3
sub folders: `data` - your actual dropbox data, `config` - data about your account. Do not delete this, `opt` where the container stores the dropbox daemon application. This is so that the container does not have to redownload the files everytime it restarts.

When you first run the container. Check the logs of the container to get a web url
to link your account.

```
gnl_dropbox $HOME/Dropbox
docker logs [container name]
...
Please visit https://www.dropbox.com/cli_link_nonce?nonce=c32f1065cd33fe8df2cbfa91d30a4e0d to link this device.
```

## gnl_vault

Create encrypted folders using `cryfs`. Cryfs is great to use in Dropbox!

```
gnl_vault   encrypted_folder   mount_point   [vault_name=default]
```

This will create a vault called `default` and mounted at `mount_point`. Any files you store in `mount_point` will be encrypted and stored in `encrypted_folder`.

If you only have one vault created, simply typing `gnl_vault` will either mount or unmount the vault.  When mounting a Vault, zenity is used to ask for your password which is passed into cryfs.

```
# Mount or unmount a vault if it is the only one created.
gnl_vault
```

All Vault data is stored in the `$HOME/.local/.gnl_vaults` folder. You can
modify these if you need

### Use gnl_dropbox and gnl_vault together!

```
# Create a hidden dropbox folder in your home direcotyr
gnl_dropbox $HOME/.dropbox
...

# Create a vault and store the encrypted files in dropbox
gnl_vault $HOME/.dropbox/data   $HOME/Vault

# Unmount the vault (mount it if it's not already mounted).
gnl_vault

```


## gnl_sftp

Run a SFTP server through docker. To run call `gnl_sftp <empty folder>`
to set up the SFTP server files.  This uses the [atmoz/sftp](https://github.com/atmoz/sftp) container.


The following files will be created:

* USERS - Edit to add new users. Each line
```
username:password:userID:groupID
```
You can use the `gen_pass.sh` script to generate an encrypted password. Encrypted passwords
must be appended with `:e`.
For example:
```
guest:guestpassword:1000:1000
bobby:$1$godaRVb9$D/5u1sQTMNfuccS6drbDT.:e:1001:1001
```

* HOST_MOUNTS - Edit to add new mounts from the Host->Container.
  Each line in the HOST_MOUTS file should be a docker volume flag:
  eg:
  ```
  -v $HOME/Documents:/media/Documents
  -v $HOME/Videos:/media/Videos
  ```

* bindmount.sh - is used to mount folders within the container. You can use this
to limit which folder a user can read. Add a new line to the bottom for each user
```
bindmount /Documents/LoveLetters/     /home/bobby/LoveLetters --read-only
bindmount /Videos/Cats/               /home/guest --read-only
```
* PORT - edit this to set the port address to host the server on.
* start.sh/stop.sh this starts and stops the sftp server.




gnl_dropbox
